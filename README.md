# ZLister Progressive Web App

A local-only "to do" application

## Design

This is a multi-page progressive web application. The user can create lists, and each list is made up of
entries. Entries can be marked as 'completed' or 'to do'. Data is stored on the local device via localStorage, but
can be imported and exported to other devices as a file. As a progressive web application, installation requires
an internet connection, but afterward interactions can be handled offline.

Pages:
- `index.html` Main entry point, create and access lists
- `list.html` Based on the URL fragment, view and edit a specific list's entries and the list itself
- `entry.html` Edit a specific entry such as rename and delete it
- `about.html` Information page about the application
- `help.html` Information about how to use the application

Typescript Modules:
- `models.ts` Common data structures, including lists (ZList) and entries (ZListEntry)
- `services.ts` Common services, including data-access objects
- `index.ts`, `list.ts`, `entry.ts` Code specific to the corresponding html pages

## Development

### Installation

Use [nvm](https://github.com/nvm-sh/nvm) to install `npm` or a similar tool. Then, `npm install` will install
`typescript` and the other dependencies.

### Testing

Run `npm test` to execute the unit tests.

Run `BASE_URL=https://zlister-pwa-jeffrey-starr-2f289c754b0fc28dfacac625951fe1313fd65.gitlab.io/ npx playwright test` to 
execute the end-to-end or browser tests after deploying. The gitlab page will represent the latest push.

To run e2e tests locally, first run a local webserver such as:

`python3 -m http.server --directory public/ 3000`

and then execute the tests:

`BASE_URL=http://localhost:3000 npx playwright test`

You can also use the codegen functionality to interactively build a test via:

`npx playwright codegen http://localhost:3000`  (or use a hosted URL)

### Generate Deployment

Run `npm run build` to populate the `public/` directory.
