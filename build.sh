#!/usr/bin/env bash

set -eu

echo "Running tests..."
npm test

echo "Checking typescript..."
tsc

echo "Bundling code..."
esbuild --bundle src/about.ts --outdir=public
esbuild --bundle src/entry.ts --outdir=public
esbuild --bundle src/index.ts --outdir=public
esbuild --bundle src/list.ts --outdir=public
esbuild --bundle src/trash.ts --outdir=public
esbuild --bundle src/sw.js --outdir=public

echo "Updating public/ with latest files"
mkdir -p public
mkdir -p public/css
mkdir -p public/img
cp -r src/css/*.css public/css/
cp -r src/img/*.png public/img/
cp -r src/img/*.svg public/img/
cp src/*.html public/
cp src/zlister.webmanifest public/

if [[ "${CI_JOB_STAGE:-nottest}" == "deploy" ]]; then
  jq '.start_url |= "https://zlister-pwa-jeffrey-starr-2f289c754b0fc28dfacac625951fe1313fd65.gitlab.io/"' public/zlister.webmanifest > public/zlister.tmp
  mv public/zlister.tmp public/zlister.webmanifest
  echo "Used test site start_url"
fi
