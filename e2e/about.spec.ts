import { test, expect } from '@playwright/test';

test('has link back to index', async ({ page }) => {
    await page.goto('about.html');

    await expect(page.getByRole('link', {name: 'Start'})).toBeVisible();
});

test('code cache populated', async ({ page, browserName }) => {
    test.skip(browserName === 'webkit', 'webkit service worker never becomes ready');
    await page.goto('index.html');  // should install service worker
    const swURL = await page.evaluate(async () => {
        const registration = await navigator.serviceWorker.ready;
        return registration.active?.scriptURL;
    });
    expect(swURL).toContain('sw.js');
    await page.goto('about.html');

    // maintenance: increase count as assets used by sw.js increase
    const codeCacheText = /application code cache contains \d+ entries/
    await expect(page.getByText(codeCacheText)).toBeVisible();
    await expect(page.getByText(codeCacheText)).toContainText('22');

    // this test is flaky so adding screenshots to help with debugging
    await page.screenshot({ path: `code-cache-screenshot-${browserName}.png`, fullPage: true });

    // maintenance: remove entries as they are controlled by sw.js
    const failedCacheText = /code cache contains \d+ entries with non-2xx status/
    await expect(page.getByText(failedCacheText)).toContainText('0');
});
