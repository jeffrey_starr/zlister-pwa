import { test, expect } from '@playwright/test';

test('add list, add entry, delete entry, delete list, hard delete entry, undelete list', async ({ page, browserName }) => {
    test.skip(browserName === 'webkit', 'webkit service never finds Name field on second page');
    await page.goto('index.html');
    await page.getByLabel('Title:').fill('List to be Deleted');
    await page.getByLabel('Title:').press('Enter');

    await page.getByLabel('Name:', { exact: true }).fill('An entry to be deleted');
    await page.getByLabel('Name:', { exact: true }).press('Enter');

    await page.getByRole('link', { name: '⌘' }).click();

    await page.getByRole('button', { name: 'Delete Entry' }).click();
    await page.getByRole('link', { name: '< Back' }).click();
    await page.getByRole('link', { name: 'List to be Deleted' }).click();
    await page.getByText('Operations', { exact: true }).click();
    await page.getByRole('button', { name: 'Delete' }).click();

    await page.getByText('Operations').click();
    await page.getByRole('link', { name: 'Manage Trash' }).click();

    await expect(page.locator('#deleted-lists-form')).toContainText('List to be Deleted');
    await expect(page.locator('#deleted-entries-form')).toContainText('List to be Deleted / An entry to be deleted');

    await page.locator('#deleted-entries-form').getByText('Hard Delete').click();
    await page.locator('#deleted-entries-form').getByRole('button', { name: 'Commit Changes' }).click();
    await page.getByText('Hard Delete').click();
    await page.getByText('Undelete').click();
    await page.getByRole('button', { name: 'Commit Changes' }).click();
    await page.getByRole('link', { name: '< Back' }).click();

    await expect(page.getByRole('listitem')).toContainText('List to be Deleted');
});
