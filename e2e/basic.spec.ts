import { test, expect } from '@playwright/test';

test('has title', async ({ page }) => {
    await page.goto('index.html');

    // Expect a title "to contain" a substring.
    await expect(page).toHaveTitle(/ZLister/);
});

test('has Your Lists section', async ({ page }) => {
   await page.goto('index.html');

   await expect(page.getByRole('heading', { name: 'Your Lists' })).toBeVisible();
});

test('has Create New List section', async ({ page }) => {
    await page.goto('index.html');

    await expect(page.getByRole('heading', { name: 'Create New List' })).toBeVisible();
});
