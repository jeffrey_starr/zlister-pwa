import { test, expect } from '@playwright/test';
import * as path from "path";

test('export zlists', async ({page, browserName}) => {
    test.skip(browserName === 'webkit', 'webkit download event never fires');
    await page.goto('index.html');
    await page.getByText('Operations').click()

    const downloadPromise = page.waitForEvent('download');
    await page.getByRole('button', {name: 'Export Database'}).click();
    const download = await downloadPromise;

    expect(download.suggestedFilename()).toStrictEqual('zlister-export.json');
});

test('import zlist', async ({page, browserName}) => {
    test.skip(browserName === 'webkit', 'webkit filechooser event never fires');
    await page.goto('index.html');
    await page.getByText('Operations').click()

    const fileChooserPromise = page.waitForEvent('filechooser');
    await page.getByRole('button', {name: 'Import Database'}).click();
    const fileChooser = await fileChooserPromise;
    await fileChooser.setFiles(path.join(__dirname, 'zlister-export.json'));

    await expect(page.locator('#import-res')).toContainText('Imported 2 entries from zlister-export.json');
});
