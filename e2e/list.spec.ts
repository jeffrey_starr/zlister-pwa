import { test, expect } from '@playwright/test';

test('create a new list and populate with 3 entries', async ({ page, browserName }) => {
    test.skip(browserName === 'webkit', 'webkit waitForURL never completes');
    await page.goto('index.html');

    const listName = 'A New List';
    await page.getByLabel('Title').fill(listName)
    await page.getByRole('button', {name: '+'}).click()
    await page.waitForURL('**/list.html#*');

    await expect(page).toHaveTitle('ZLister - list');
    await expect(page.getByRole('heading', {level: 1})).toContainText(listName);

    const entryNames = ['Entry A', 'Entry B', 'Entry C'];
    for (let entryName of entryNames) {
        await page.getByLabel('Name:', {exact: true}).fill(entryName);
        await page.getByRole('button', {name: '+'}).click();
    }

    await expect(page.getByRole('listitem')).toContainText(entryNames);
});

test('rename a list', async ({ page, browserName }) => {
    test.skip(browserName === 'webkit', 'webkit cannot find the New Name label');
    await page.goto('index.html');

    await page.getByLabel('Title:').fill('My New Lists');
    await page.getByLabel('Title:').press('Enter');

    await page.getByText('Operations', { exact: true }).click();
    await page.getByLabel('New Name:').fill('My New Singular List');
    await page.getByLabel('New Name:').press('Enter');

    await expect(page.locator('#list-title')).toContainText('My New Singular List');
});
