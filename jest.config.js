/** @type {import('ts-jest').JestConfigWithTsJest} */
module.exports = {
  roots: ['<rootDir>/src/', '<rootDir>/test/'],
  preset: 'ts-jest/presets/default-esm',
  testEnvironment: 'jsdom',
  moduleDirectories: ['<rootDir>/../', 'node_modules'],
  collectCoverage: true,
  coveragePathIgnorePatterns: ["/node_modules/", "/test/defs.ts", "test/ShimStorage.ts"]
};
