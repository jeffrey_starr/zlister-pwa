import {CacheStorageSvc, CacheSvc} from "./services";


async function onDOMContentLoaded(cacheSvc: CacheSvc): Promise<void> {
    const count = await cacheSvc.size();
    document.getElementById('cache-count').textContent = count.toString();

    try {
        const youngest = await cacheSvc.youngest();
        document.getElementById('cache-youngest-date').textContent = youngest.toLocaleString();
    } catch (err) {
        if (count === 0) {
            document.getElementById('cache-youngest-date').textContent = '(no entries, so no youngest)';
        } else {
            document.getElementById('cache-youngest-date').textContent = `Error ${err}`;
        }
    }

    const fails = await cacheSvc.failedResponses();
    document.getElementById('cache-failed-entries-count').textContent = fails.length.toString();

    if(fails.length > 0) {
        const ol = document.getElementById('cache-failed-entries');
        for(let failedResponse of fails) {
            const li = document.createElement('li');
            li.textContent = `${failedResponse.url} (${failedResponse.status})`
            ol.appendChild(li);
        }
    }

    const clearCacheButton = document.getElementById('clear-cache-btn');
    clearCacheButton.addEventListener('click', async () => {
        const cacheFound = await cacheSvc.clear();
        const registrations = await navigator.serviceWorker.getRegistrations();
        for(let registration of registrations) {
            await registration.unregister();
        }

        if(cacheFound) {
            alert('Cache cleared');
        } else {
            alert('Cache not found. Already cleared?');
        }
    });
}

/**
 * Initialize services (in a browser context)
 */
function main() {
    const cacheSvc: CacheSvc = new CacheStorageSvc(window.caches);

    if(document.readyState === 'loading') {
        document.addEventListener('DOMContentLoaded', (_) => {
            void onDOMContentLoaded(cacheSvc);
        });
    } else {
        void onDOMContentLoaded(cacheSvc);
    }
}

// launch page initialization if we are running in a browser
if(!(navigator.userAgent.includes("Node.js") || navigator.userAgent.includes("jsdom"))) {
    main();
}
