import {DefaultZListMgmt, StorageZListDAO, ZListMgmt} from "./services";
import {ZList, ZListEntry} from "./models";

function onDOMContentLoaded(zListMgmt: ZListMgmt, zList: ZList, entry: ZListEntry): void {
    document.getElementById('list-title').textContent = entry.content;

    document.getElementById('back-link').setAttribute('href', `list.html#${zList.id}`);

    const renameForm = document.getElementById('rename-form');
    renameForm.addEventListener('submit', (evt: SubmitEvent) => {
        evt.preventDefault();
        const newNameInput = document.getElementById('rename');

        if(newNameInput instanceof HTMLInputElement) {
            entry.content = newNameInput.value;
            entry.lastUpdated = Date.now();
            const updated = zListMgmt.sync(zList.id, entry);
            if(updated instanceof Error) {
                document.getElementById('rename-error-messsage').textContent = `Error: ${updated.message}`;
            } else {
                window.location.assign(`list.html#${zList.id}`);
            }
        } else {
            document.getElementById('rename-error-messsage').textContent = 'Element rename incorrectly typed';
        }
    });

    const deleteBtn = document.getElementById('delete-btn');
    deleteBtn.addEventListener('click', () => {
        entry.isDeleted = true;
        entry.lastUpdated = Date.now();
        zListMgmt.sync(zList.id, entry);
        window.location.assign(`list.html#${zList.id}`);
    });
}

function main() {
    const dao = new StorageZListDAO({'storage': window.localStorage, 'target': window});
    const zlistMgmt = new DefaultZListMgmt(dao);
    const loc = (location.hash || '#').slice(1).split('/');
    const zListId = loc[0];
    const entryId = loc[1];

    if(document.readyState === 'loading') {
        document.addEventListener('DOMContentLoaded', (_) => {
            const zlist = zlistMgmt.list(zListId);
            const entry = zlist.entry(entryId);
            onDOMContentLoaded(zlistMgmt, zlist, entry);
        });
    } else {
        const zlist = zlistMgmt.list(zListId);
        const entry = zlist.entry(entryId);
        onDOMContentLoaded(zlistMgmt, zlist, entry);
    }
}

// launch page initialization if we are running in a browser
if(!(navigator.userAgent.includes("Node.js") || navigator.userAgent.includes("jsdom"))) {
    main();
}
