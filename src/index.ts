import {ZList} from "./models";
import {DefaultZListMgmt, PersistencePolicy, StorageZListDAO, ZListMgmt} from "./services";


async function setupPersistenceControl(policy: PersistencePolicy): Promise<void> {
    const policyDesc = document.getElementById('quota-desc')
    const positive = 'Your list data will only be deleted explicitly.'

    const willPersist = await policy.browserWillAvoidDeletion();
    if(willPersist) {
        policyDesc.textContent = positive;
    } else {
        const mesg = document.createElement('span');
        mesg.textContent = 'Your list data may be deleted by the browser under storage pressure. '
        const btn = document.createElement('button');
        btn.textContent = 'Request Data Preservation';
        btn.addEventListener('click', async () => {
            const given = await policy.promptToAvoidDeletion();
            if(given) {
                policyDesc.textContent = positive;
            }
        });

        policyDesc.textContent = '';
        policyDesc.appendChild(mesg);
        policyDesc.appendChild(btn);
    }
}

async function onDOMContentLoaded(policy: PersistencePolicy, zlistMgmt: ZListMgmt) {
    await setupPersistenceControl(policy);

    populateLists(document.getElementById('zlist-lists-menu'), zlistMgmt.lists(false));

    document.getElementById('new-zlist-form').addEventListener('submit', (evt: SubmitEvent) => {
        evt.preventDefault();
        const titleInput = document.getElementById('zlist-title');

        if(titleInput && titleInput instanceof HTMLInputElement) {
            // Automatic HTML form validation will happen prior to the submit being fired
            const zlistOrError = zlistMgmt.makeNewList(titleInput.value);
            if(zlistOrError instanceof ZList) {
                window.location.assign(`list.html#${zlistOrError.id}`);
            } else if(zlistOrError instanceof Error) {
                const dst = document.getElementById('new-zlist-form-error');
                dst.textContent = zlistOrError.message;
            }
        } else {
            throw new Error('zlist-title element either does not exist or is not a `input` type');
        }
    });

    document.getElementById('export-btn').addEventListener('click', async () => {
        const content = zlistMgmt.export();
        const blob = new Blob([content], {type: 'application/json'});
        const exportURL = URL.createObjectURL(blob);
        // Using the 'classic' approach as described in https://web.dev/patterns/files/save-a-file/
        // since the chooseSaveFilePicker API is unlikely to be supported in Firefox (https://mozilla.github.io/standards-positions/#native-file-system)
        const a = document.createElement('a');
        a.href = exportURL;
        a.download = 'zlister-export.json';
        a.style.display = 'none';
        document.body.append(a);
        a.click();
        setTimeout(() => {
            URL.revokeObjectURL(exportURL);
            a.remove();
        }, 1000);
    });

    document.getElementById('import-btn').addEventListener('click', async () => {
        console.log('import clicked');
        const importMsg = document.getElementById('import-res');
        const errorMsg = document.getElementById('import-err');
        importMsg.textContent = '';  // clear both on start
        errorMsg.textContent = '';

        // Classic method: https://web.dev/patterns/files/open-one-or-multiple-files
        const input = document.createElement('input');
        input.style.display = 'none';
        input.type = 'file';
        input.accept = 'application/json,*.json,*.js';
        document.body.append(input);
        input.addEventListener('change', async () => {
            input.remove();
            if (!input.files) {
                return;
            } else {
                try {
                    const importFile = input.files[0];
                    const content = await importFile.text();
                    const result = zlistMgmt.import(content)
                    if (result.errors.length > 0) {
                        errorMsg.textContent = result.errors.map((err) => (err.message)).join('\n')
                    }
                    importMsg.textContent = `Imported ${result.imported} entries from ${importFile.name}`
                } catch (err) {
                    errorMsg.textContent = err.message;
                }
            }
        });
        input.click();
    });
}

function populateLists(menu: HTMLElement, zlists: ZList[]) {
    // <li class="toplevel"><a href="list.html#234sf...">Shopping</a></li>
    if(zlists.length == 0) {
        const child = document.createElement('li')
        child.setHTMLUnsafe('<span>(None - Create a list below)</span>');
        menu.appendChild(child);
    } else {
        zlists.forEach((zlist) => {
            const child = document.createElement('li');
            child.setAttribute('class', 'toplevel');

            const link = document.createElement('a');
            link.href = `list.html#${zlist.id}`;
            link.textContent = zlist.title;

            child.appendChild(link);
            menu.appendChild(child);
        });
    }
}

/**
 * Initialize services (in a browser context)
 */
function main() {
    const quotaSvc = new PersistencePolicy(navigator.storage);
    const dao = new StorageZListDAO({'storage': window.localStorage, 'target': window});
    const zlistMgmt = new DefaultZListMgmt(dao);

    if(document.readyState === 'loading') {
        document.addEventListener('DOMContentLoaded', (_) => {
            onDOMContentLoaded(quotaSvc, zlistMgmt);
        });
    } else {
        onDOMContentLoaded(quotaSvc, zlistMgmt);
    }
}

// launch page initialization if we are running in a browser
if(!(navigator.userAgent.includes("Node.js") || navigator.userAgent.includes("jsdom"))) {
    main();
    if ('serviceWorker' in navigator) {
        navigator.serviceWorker.register('./sw.js').then(
            (_: ServiceWorkerRegistration) => {
                console.log('Service worker registration successful');
            },
            (error) => {
                console.error('Service worker registration failed', error);
            }
        )
    } else {
        console.error('Service workers are not supported; offline experience will fail');
    }
}
