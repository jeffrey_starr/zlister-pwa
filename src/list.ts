import {DefaultZListMgmt, StorageZListDAO, ZListMgmt} from "./services";
import {ZList, ZListEntry} from "./models";

function entryListItem(zlistId: string, entry: ZListEntry): HTMLLIElement {
    // <li class="entry">
    //   <form>
    //     <input id="5" type="checkbox" checked/> <label for="5">Ice Cream</label>
    //   </form>
    //   <span class="control">⌘</span>
    // </li>
    const li = document.createElement('li');
    li.setAttribute('class', 'entry');
    li.setAttribute('id', entry.id);

    const form = document.createElement('form')

    const checkbox = document.createElement('input');
    checkbox.setAttribute('name', 'entry-check');
    checkbox.setAttribute('type', 'checkbox');
    checkbox.checked = entry.isChecked
    form.appendChild(checkbox);

    checkbox.addEventListener('change', (evt) => {
        moveEntry(entry, checkbox.checked);
    });

    const label = document.createElement('label');
    label.setAttribute('for', 'entry-check');
    label.textContent = entry.content;
    form.appendChild(label);

    const hiddenIsDeleted = document.createElement('input');
    hiddenIsDeleted.setAttribute('type', 'hidden');
    hiddenIsDeleted.setAttribute('name', 'isDeleted');
    hiddenIsDeleted.value = Boolean(entry.isDeleted).toString();
    form.appendChild(hiddenIsDeleted);

    const hiddenLastUpdated = document.createElement('input');
    hiddenLastUpdated.setAttribute('type', 'hidden');
    hiddenLastUpdated.setAttribute('name', 'lastUpdated');
    hiddenLastUpdated.value = entry.lastUpdated.toString();
    form.appendChild(hiddenLastUpdated);

    li.appendChild(form);

    const entryEdit = document.createElement('span');
    entryEdit.setAttribute('class', 'control');
    const editLink = document.createElement('a');
    editLink.setAttribute('href', `entry.html#${zlistId}/${entry.id}`)
    editLink.textContent = '⌘';  // Unicode Place of Interest Sign; https://en.wikipedia.org/wiki/Looped_square
    entryEdit.appendChild(editLink);

    li.appendChild(entryEdit);

    return li;
}

function extractListItem(li: HTMLLIElement): ZListEntry {
    const entryId = li.getAttribute('id');
    const content = li.querySelector('label[for=entry-check]').textContent;
    const checkboxI = li.querySelector('input[name=entry-check]');
    const deletedI = li.querySelector('input[name=isDeleted]');
    const lastUpdatedI = li.querySelector('input[name=lastUpdated]');

    if(checkboxI instanceof HTMLInputElement && deletedI instanceof HTMLInputElement && lastUpdatedI instanceof HTMLInputElement) {
        const props = {
            'id': entryId,
            'content': content,
            'isChecked': checkboxI.checked,
            'isDeleted': deletedI.checked,
            'lastUpdated': new Date(Number.parseInt(lastUpdatedI.value))
        }
        return new ZListEntry(props)
    } else {
        console.error('extractListItem: Elements are not of the expected type')
    }

}

function populateEntryMenus(zlistId: string, menu: HTMLMenuElement, entries: ZListEntry[]) {
    entries.forEach( (e) => {
        menu.appendChild(entryListItem(zlistId, e));
    });
}

function reportErrorToUser(message: string): void {
    const e = document.getElementById('new-entry-error');
    if(!e) {
        console.error(`Failed to find element new-entry-error; unable to report message ${message}`);
    }

    e.textContent = message;
}

function onDOMContentLoaded(zlistId: string, zlistMgmt: ZListMgmt) {
    const zlist = zlistMgmt.list(zlistId);
    const titleElem = document.getElementById('list-title');

    if(!zlistId) {
        titleElem.textContent = 'No id fragment given / navigation error';
        return;
    } else if(!zlist) {
        titleElem.textContent = `No list found for id ${zlistId}`;
        return;
    } else {
        titleElem.textContent = zlist.title;
        document.getElementById('rename-list').nodeValue = zlist.title;
    }

    const observer = new MutationObserver((records) => {
        onMenuChanged(zlistId, zlistMgmt, records);
    });
    const observerOptions = {'childList': true};

    let menu1 = document.getElementById('to-do-menu');
    if(menu1 instanceof HTMLMenuElement) {
        populateEntryMenus(zlistId, menu1, zlist.toDo());
        observer.observe(menu1, observerOptions);
    } else {
        reportErrorToUser('The element to-do-menu is not of type menu');
        return;
    }
    let menu2 = document.getElementById('completed-menu');
    if(menu2 instanceof HTMLMenuElement) {
        populateEntryMenus(zlistId, menu2, zlist.completed());
        observer.observe(menu2, observerOptions);
    } else {
        reportErrorToUser('The element completed-menu is not of type menu');
        return;
    }

    document.getElementById('new-entry-form').addEventListener('submit', (evt: SubmitEvent) => {
        evt.preventDefault();
        const nameInput = document.getElementById('entry-title');

        if(nameInput && nameInput instanceof HTMLInputElement) {
            const entry = new ZListEntry({'content': nameInput.value});
            populateEntryMenus(zlistId, menu1, [entry]);
            const parentForm = document.getElementById('new-entry-form');
            if(parentForm instanceof HTMLFormElement) {
                parentForm.reset();
            }
        } else {
            reportErrorToUser('Element entry-title either does not exist or is not an HTML input type');
        }
    });

    document.getElementById('rename-list-form').addEventListener('submit', (evt: SubmitEvent) => {
        evt.preventDefault();
        const newNameInput = document.getElementById('rename-list');

        if(newNameInput instanceof HTMLInputElement) {
            zlist.title = newNameInput.value;
            zlistMgmt.upsert(zlist);
            window.location.reload();
        }
    });

    document.getElementById('delete-list-form').addEventListener('submit', (evt: SubmitEvent) => {
        evt.preventDefault();

        zlist.isDeleted = true;
        zlistMgmt.upsert(zlist);
        window.location.assign('index.html');
    });
}

function onMenuChanged(zlistId: string, zListMgmt: ZListMgmt, records: MutationRecord[]) {
    const zlist = zListMgmt.list(zlistId);

    // NB: creating a new entry first populates the dom, then this callback will be invoked, so
    // an entry may not exist in the database yet
    for(let record of records) {
        if (record.target instanceof HTMLMenuElement) {
            const isToDo = record.target.getAttribute('id') === 'to-do-menu';
            const isDone = record.target.getAttribute('id') === 'completed-menu';

            for(let addedLI of record.addedNodes) {
                if(addedLI instanceof HTMLLIElement) {
                    const newEntryId = addedLI.getAttribute('id').slice(3); // skip li- prefix
                    const entry = extractListItem(addedLI);
                    const syncStatus = zListMgmt.sync(zlistId, entry);

                }
            }

        } else {
            reportErrorToUser(`record.target is not an HTMLMenuElement but ${record.target.nodeType}`);
        }
    }
}

function moveEntry(entry: ZListEntry, hasCompleted: boolean) {
    let src: HTMLElement = null;
    let dst: HTMLElement = null;
    let animation = '';

    if(hasCompleted) {
        src = document.getElementById('to-do-menu');
        dst = document.getElementById('completed-menu');
        animation = 'animate__fadeOutDown'
    } else {
        src = document.getElementById('completed-menu');
        dst = document.getElementById('to-do-menu');
        animation = 'animate__fadeOutUp'
    }

    const li = document.getElementById(entry.id);
    li.classList.add(animation);

    dst.appendChild(li);  // appending an existing element to a new parent effects a move
}

/**
 * Initialize services (in a browser context)
 */
function main() {
    const dao = new StorageZListDAO({'storage': window.localStorage, 'target': window});
    const zlistMgmt = new DefaultZListMgmt(dao);
    const zListId = (location.hash || '#').slice(1);

    if(document.readyState === 'loading') {
        document.addEventListener('DOMContentLoaded', (_) => {
            onDOMContentLoaded(zListId, zlistMgmt);
        });
    } else {
        onDOMContentLoaded(zListId, zlistMgmt);
    }
}

// launch page initialization if we are running in a browser
if(!(navigator.userAgent.includes("Node.js") || navigator.userAgent.includes("jsdom"))) {
    main();
}
