import {v4 as uuidv4, validate as uuidvalidate} from 'uuid';

export class ZListEntry {
    id: string;
    content: string;
    isChecked: boolean;
    isDeleted: boolean;
    lastUpdated: number;  // epoch ms

    constructor(props: object) {
        if ('content' in props && typeof props['content'] === 'string' && props['content'].length > 0) {
            this.content = props['content'];
        } else {
            throw new Error('props.content must be a non-empty string');
        }

        this.id = props['id'] || uuidv4();
        if (!uuidvalidate(this.id)) {
            throw new Error('id is not a valid UUID string')
        }

        this.isChecked = props['isChecked'] || false;
        this.isDeleted = props['isDeleted'] || false;
        const dateish = props['lastUpdated'];
        if (typeof dateish === 'number') {
            this.lastUpdated = dateish;
        } else if (dateish instanceof Date) {
            this.lastUpdated = dateish.getTime();
        } else {
            this.lastUpdated = Date.now();
        }
    }

    serialize(): string {
        return JSON.stringify(this);
    }

}

export class ZList {
    id: string;
    title: string;
    children: ZListEntry[];
    isDeleted: boolean;

    constructor(props: object) {
        if ('title' in props && typeof props['title'] === 'string' && props['title'].length > 0) {
            this.title = props['title'];
        } else {
            throw new Error('props.content must be a non-empty string');
        }

        this.id = props['id'] || uuidv4();
        if (!uuidvalidate(this.id)) {
            throw new Error('id is not a valid UUID string')
        }

        this.isDeleted = props['isDeleted'] || false;

        if ('children' in props && Array.isArray(props['children'])) {
            this.children = props['children'].map((p) => new ZListEntry(p));
        } else if ('children' in props) {
            throw new Error('props.children must be an array of serialized ZListEntry');
        } else {
            this.children = [];
        }
    }

    serialize(): string {
        return JSON.stringify(this);
    }

    completed(): ZListEntry[] {
        return this.children.filter( (e) => { return e.isChecked && !e.isDeleted });
    }

    deleted(): ZListEntry[] {
        return this.children.filter( (e) => { return e.isDeleted });
    }

    toDo(): ZListEntry[] {
        return this.children.filter( (e) => { return !e.isChecked && !e.isDeleted });
    }

    entry(entryId: string): ZListEntry | null {
        return this.children.find( (e) => { return e.id === entryId }) || null;
    }

    upsert(child: ZListEntry): void {
        const idx = this.children.findIndex( (e) => { return e.id === child.id });
        if (idx < 0) {
            this.children.push(child);
        } else {
            this.children[idx] = child;
        }
    }
}