import {ZList, ZListEntry} from "./models";
import {CacheStorage} from "../test/defs";

export interface ZListDAO extends EventTarget {
    lists(): ReadonlyArray<ZList>;

    getList(zlistId: string): ZList | null;

    upsertList(zlist: ZList): void;

    deleteList(zlistId: string): void;
}

/**
 * Type: `zliststorageevent`
 */
export class ZListStorageEvent extends Event {
    // The id of the impacted ZList or null if the ZList was deleted
    readonly zlistId?: string;

    // The newly updated or inserted ZList or null if the ZList was deleted
    readonly newValue?: ZList;

    // The previous value or null if the ZList was just inserted
    readonly oldValue?: ZList;

    constructor(zlistId?: string, newValue?: ZList, oldValue?: ZList) {
        super('zliststorageevent');
        this.zlistId = zlistId;
        this.newValue = newValue;
        this.oldValue = oldValue;
    }
}

export class StorageZListDAO extends EventTarget implements ZListDAO {

    private storage: Storage;

    constructor(props) {
        super();
        this.storage = props['storage'];
        const self = this;
        props['target'].addEventListener('storage', (evt: StorageEvent) => {
            const ze = new ZListStorageEvent(evt.key,
                evt.newValue ? new ZList(JSON.parse(evt.newValue)) : null,
                evt.oldValue ? new ZList(JSON.parse(evt.oldValue)) : null);
            self.dispatchEvent(ze);
        });
    }

    deleteList(zlistId: string): void {
        this.storage.removeItem(zlistId);
    }

    getList(zlistId: string): ZList | null {
        const maybeZlist = this.storage.getItem(zlistId);
        if (maybeZlist) {
            return new ZList(JSON.parse(maybeZlist));
        } else {
            return null;
        }
    }

    lists(): ReadonlyArray<ZList> {
        const len = this.storage.length;
        const li: ZList[] = Array<ZList>();

        for (let i = 0; i < len; i++) {
            const key: string | null = this.storage.key(i)
            if (key) {
                const s = this.storage.getItem(key);
                try {
                    li.push(new ZList(JSON.parse(s)));
                } catch (err) {

                }
            } else {
                console.log(`No key found for index ${i} in storage. Should not happen; ignoring index`);
            }
        }
        return li;
    }

    upsertList(zlist: ZList): void {
        this.storage.setItem(zlist.id, zlist.serialize());
    }

}

/**
 * Identify policy around deleting stored data if there is storage space pressure
 */
export class PersistencePolicy {
    private manager: StorageManager;

    constructor(manager: StorageManager) {
        this.manager = manager;
    }

    browserWillAvoidDeletion(): Promise<boolean> {
        return this.manager.persisted();
    }

    promptToAvoidDeletion(): Promise<boolean> {
        return this.manager.persist();
    }
}

export class ImportResult {
    // successful imports
    imported: number

    // errors encountered during import
    errors: Error[]

    constructor(count?: number, errors?: Error[]) {
        this.imported = count || 0;
        this.errors = errors || [];
    }
}

export interface ZListMgmt {
    export(): string

    import(content: string): ImportResult

    makeNewEntry(zlist: string, name: string): ZListEntry | Error

    makeNewList(title: string): ZList | Error

    list(zlistId: string): ZList | null

    lists(includeDeleted: boolean): ZList[]

    sync(zlistId: string, entry: ZListEntry): boolean | Error

    upsert(zList: ZList): void
}

export class DefaultZListMgmt implements ZListMgmt {

    dao: ZListDAO;

    constructor(dao: ZListDAO) {
        this.dao = dao;
    }

    export(): string {
        return JSON.stringify({zLists: this.lists(true)});
    }

    import(content: string): ImportResult {
        const field = 'zLists';

        try {
            const j = JSON.parse(content);
            if (j instanceof Object && field in j && Array.isArray(j[field])) {
                const zl = j[field];
                const result = new ImportResult();

                for(let possible of zl) {
                    try {
                        const zList = new ZList(possible)
                        this.dao.upsertList(zList);
                        result.imported = result.imported + 1;
                    } catch (err) {
                        result.errors.push(err);
                    }
                }

                return result;
            } else {
                return new ImportResult(0, [new Error('zLists property does not exist or is not an array')]);
            }
        } catch (err) {
            return new ImportResult(0, [err]);
        }
    }

    list(zlistId: string): ZList | null {
        return this.dao.getList(zlistId)
    }

    lists(includeDeleted: boolean = false): ZList[] {
        let l = Array.from(this.dao.lists());
        if (!includeDeleted) {
            l = l.filter((z) => {
                return !z.isDeleted
            });
        }
        l.sort((a: ZList, b: ZList) => {
            const first = a.title.localeCompare(b.title);
            if (first == 0) {
                return a.id.localeCompare(b.id);
            } else {
                return first;
            }
        });

        return l;
    }

    makeNewList(title: string): ZList | Error {
        try {
            const zlist = new ZList({'title': title});
            this.dao.upsertList(zlist);
            return zlist;
        } catch (err) {
            return err;
        }
    }

    makeNewEntry(zlistId: string, name: String): ZListEntry | Error {
        const entry = new ZListEntry({'content': name});
        const zlist = this.list(zlistId);
        if (zlist) {
            zlist.children.push(entry);
            this.dao.upsertList(zlist);
            return entry;
        } else {
            return Error(`No ZList with id ${zlistId} exists`);
        }
    }

    sync(zlistId: string, entry: ZListEntry): boolean | Error {
        const zlist = this.list(zlistId);
        if (zlist instanceof ZList) {
            zlist.upsert(entry);
            this.dao.upsertList(zlist);
            return true;
        } else {
            return Error('zlist for zlistid not found');
        }
    }

    upsert(zList: ZList): void {
        this.dao.upsertList(zList);
    }
}

export interface CacheSvc {
    clear(): Promise<boolean>

    size(): Promise<number>

    failedResponses(): Promise<Response[]>

    youngest(): Promise<Date>
}

export class CacheStorageSvc implements CacheSvc {
    cache: Promise<Cache>;
    storage: CacheStorage;
    cacheName = 'zlister';

    constructor(storage: CacheStorage) {
        this.cache = storage.open(this.cacheName);
        this.storage = storage;
    }

    clear(): Promise<boolean> {
        return this.storage.delete(this.cacheName);
    }

    failedResponses(): Promise<Response[]> {
        return this.cache.then((cache) => {
            return cache.matchAll().then((all: Response[]) => {
                return all.filter((resp) => {
                    return !resp.ok
                });
            });
        });
    }

    size(): Promise<number> {
        return this.cache.then((cache) => {
            return cache.keys().then((requests: Request[]) => {
                return requests.length;
            });
        });
    }

    youngest(): Promise<Date> {
        return this.cache.then((cache) => {
            return cache.matchAll().then((all: Response[]) => {
                if (all.length === 0) {
                    throw Error('(Youngest does not exist; no cache entries)');
                } else {
                    const sorted = Array.from(all).sort((a, b) => {
                        const errOrDateA = CacheStorageSvc.httpDateToDate(a.headers.get('Date') || '');
                        const errOrDateB = CacheStorageSvc.httpDateToDate(b.headers.get('Date') || '');
                        // sort errors before proper dates
                        if (errOrDateA instanceof Date && errOrDateB instanceof Date) {
                            return errOrDateA.valueOf() - errOrDateB.valueOf();
                        } else if (errOrDateA instanceof Date) {
                            return 1;
                        } else if (errOrDateB instanceof Date) {
                            return -1;
                        } else {
                            return 0;
                        }
                    });

                    const youngest = CacheStorageSvc.httpDateToDate(sorted.pop().headers.get('Date') || '');
                    if (youngest instanceof Error) {
                        throw Error('Latest response lacks a Date header or the header is invalid');
                    } else {
                        return youngest;
                    }
                }
            });
        })
    }

    // Convert the date in an HTTP header to a Javascript Date
    // https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Date
    static httpDateToDate(header: string): Date | Error {
        // Date: <day-name>, <day> <month> <year> <hour>:<minute>:<second> GMT
        const pattern = /\w\w\w, (\d\d) (\w\w\w) (\d\d\d\d) (\d\d):(\d\d):(\d\d) GMT/;
        const match = pattern.exec(header);
        if(match === null) {
            return Error('Invalid format: date string does not match expected pattern');
        } else {
            const day = Number.parseInt(match[1], 10);
            const monthIndex = this.months.indexOf(match[2]);
            const year = Number.parseInt(match[3], 10);
            const hour = Number.parseInt(match[4], 10);
            const minute = Number.parseInt(match[5], 10);
            const second = Number.parseInt(match[6], 10);

            return new Date(year, monthIndex, day, hour, minute, second);
        }
    }

    private static months = [
        "Jan", "Feb", "Mar", "Apr", "May", "Jun",
        "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

}