// Why isn't this a typescript file? https://github.com/Microsoft/TypeScript/issues/20595 Since the functionality
// is small and straight-forward, we can forgo type checking and avoid the complexities of splitting the project code.

// Resources to be cached to enable off-line experience. If a resource is missing from this list, the application
// will attempt to load it from the network.
const cacheList = [
    '/',
    '/about.html',
    '/about.js',
    '/entry.html',
    '/entry.js',
    '/help.html',
    '/index.html',
    '/index.js',
    '/list.html',
    '/list.js',
    '/trash.html',
    '/trash.js',
    '/css/normalize.css',
    '/css/style.css',
    '/img/icon-44.png',
    '/img/icon-128.png',
    '/img/icon-192.png',
    '/img/icon-512.png',
    '/img/icon-1024.png',
    '/img/icon-1024.svg',
    '/img/zlister-index-small.png',
    '/img/zlister-shopping-small.png'
];

const cacheName = 'zlister';

function onInstall(install) {
    install.waitUntil(caches.open(cacheName).then((cache) => {
        return cache.addAll(cacheList).then(() => {
            console.log('Service Worker: cache populated');
        });
    }));
}

function onFetch(fe) {
    if(!fe.request.url.startsWith('http:') && !fe.request.url.startsWith('https:')) {
        return;  // avoid file: and extensions: handling
    }

    fe.respondWith((async () => {
        const r = await caches.match(fe.request, {ignoreVary: true});
        if (r) {
            return r;
        } else {
            console.warn(`Failed to find ${fe.request.url} in cache; fetching from network`);
            const resp = await fetch(fe.request);
            const cache = await caches.open(cacheName);
            await cache.put(fe.request, resp.clone());
            return resp;
        }
    })());
}

self.addEventListener('install', (install) => onInstall(install));
self.addEventListener('fetch', (fetch) => onFetch(fetch));

