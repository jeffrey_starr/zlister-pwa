import {StorageZListDAO, ZListDAO} from "./services";
import {ZList, ZListEntry} from "./models";

/**
 * Build a fieldset for hard deleting, undeleting, or not changing a data element
 * @param title fieldset label content (e.g. list title)
 * @param elemName name attribute of radio group
 */
function deletedXNode(title: string, elemName: string): HTMLFieldSetElement {
    const fieldset = document.createElement('fieldset');

    const legend = document.createElement('legend');
    legend.textContent = title;
    fieldset.appendChild(legend);

    const options = [{t: 'No Change', v: 'no-change'}, {t: 'Undelete', v: 'undelete'}, {t: 'Hard Delete', v: 'hard'}];
    for(let option of options) {
        const div = document.createElement('div');
        const label = document.createElement('label');
        const input = document.createElement('input');
        input.type = 'radio';
        input.name = elemName;
        input.value = option.v;
        if(option.v === 'no-change') {
            input.checked = true;
        }
        label.appendChild(input);
        label.append(` ${option.t}`);
        div.append(label);
        fieldset.appendChild(div);
    }

    return fieldset;
}

type ParentChild = [ZList, ZListEntry];

function deletedEntries(lists: readonly ZList[]): ParentChild[] {
    const pairs: ParentChild[] = []

    for(let list of lists) {
        for(let deleted of list.deleted()) {
            pairs.push([list, deleted]);
        }
    }

    return pairs;
}

function updateDeletedListStatus(dao: ZListDAO, deletedLists: ZList[]): ((evt: SubmitEvent) => void) {
    return function (evt: SubmitEvent): void {
        evt.preventDefault();
        const form = document.getElementById('deleted-lists-form');

        if(form instanceof HTMLFormElement) {
            for(let deletedList of deletedLists) {
                const radioGroup = form.elements.namedItem(deletedList.id)
                if(radioGroup instanceof RadioNodeList) {
                    const selected = radioGroup.value;

                    switch (selected) {
                        case 'hard':
                            dao.deleteList(deletedList.id);
                            break;
                        case 'no-change':
                            // do nothing
                            break;
                        case 'undelete':
                            deletedList.isDeleted = false;
                            dao.upsertList(deletedList);
                            break;
                        default:
                            console.error(`Unexpected selected value of ${deletedList.title}: ${selected}`);
                    }
                }
            }
        }

        window.location.reload();
    }
}

function updateDeletedEntriesStatus(dao: ZListDAO, deletedEntries: ParentChild[]): ((evt: SubmitEvent) => void) {
    return function (evt: SubmitEvent): void {
        evt.preventDefault();
        const form = document.getElementById('deleted-entries-form');

        if(form instanceof HTMLFormElement) {
            for(let pair of deletedEntries) {
                const radioGroup = form.elements.namedItem(pair[1].id);
                if(radioGroup instanceof RadioNodeList) {
                    const selected = radioGroup.value;

                    switch (selected) {
                        case 'hard':
                            const keepList = dao.getList(pair[0].id);
                            const entryToBeDeleted = keepList.entry(pair[1].id);
                            keepList.children.splice(keepList.children.indexOf(entryToBeDeleted), 1);
                            dao.upsertList(keepList);
                            break;
                        case 'no-change':
                            // do nothing
                            break;
                        case 'undelete':
                            const latestList = dao.getList(pair[0].id);
                            const latestEntry = latestList.entry(pair[1].id);
                            latestEntry.isDeleted = false;
                            latestList.upsert(latestEntry);
                            dao.upsertList(latestList);
                            break;
                        default:
                            console.error(`Unexpected selected value of ${pair[1].content}: ${selected}`);
                    }
                }
            }
        }

        window.location.reload();
    }
}

function onDOMContentLoaded(dao: ZListDAO): void {
    const deletedLists = dao.lists().filter((zList) => { return zList.isDeleted });

    document.getElementById('deleted-lists-info').textContent = `${deletedLists.length} deleted lists found`;

    const deletedListsForm = document.getElementById('deleted-lists-form');
    if(deletedLists.length > 0 && deletedListsForm instanceof HTMLFormElement) {
        for(let deletedList of deletedLists) {
            deletedListsForm.appendChild(deletedXNode(deletedList.title, deletedList.id));
        }

        const submit = document.createElement('input')
        submit.type = 'submit';
        submit.value = 'Commit Changes';
        deletedListsForm.append(submit);

        deletedListsForm.addEventListener('submit', updateDeletedListStatus(dao, deletedLists));
    }

    // We could further restrict the list by only showing deleted entries whose parent is not-deleted, but that may
    // be confusing. Undeleting an entry whose parent is deleted (and thus does not reappear in the UI) may also be
    // confusing, but at least the simplest approach is explicit.
    const deletedPairs = deletedEntries(dao.lists());

    const deletedEntriesForm = document.getElementById('deleted-entries-form');
    if(deletedPairs.length > 0 && deletedEntriesForm instanceof HTMLFormElement) {
        for(let deletedPair of deletedPairs) {
            deletedEntriesForm.appendChild(deletedXNode(`${deletedPair[0].title} / ${deletedPair[1].content}`, deletedPair[1].id));
        }

        const submit = document.createElement('input')
        submit.type = 'submit';
        submit.value = 'Commit Changes';
        deletedEntriesForm.append(submit);

        deletedEntriesForm.addEventListener('submit', updateDeletedEntriesStatus(dao, deletedPairs));
    }
}

function main() {
    const dao = new StorageZListDAO({'storage': window.localStorage, 'target': window});

    if(document.readyState === 'loading') {
        document.addEventListener('DOMContentLoaded', (_) => {
            onDOMContentLoaded(dao);
        });
    } else {
        onDOMContentLoaded(dao);
    }
}

// launch page initialization if we are running in a browser
if(!(navigator.userAgent.includes("Node.js") || navigator.userAgent.includes("jsdom"))) {
    main();
}
