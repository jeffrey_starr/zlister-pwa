
export class ShimStorage extends EventTarget implements Storage {
    length: number;

    // NB: Map iterates keys in insertion order
    private kv: Map<string, string>;

    constructor() {
        super();
        this.kv = new Map();
        this.length = this.kv.size;
    }

    protected emitDeleted(key: string, oldValue: string) {
        const evt = new StorageEvent('storage', {'key': null, 'newValue': null, 'oldValue': oldValue});
        this.dispatchEvent(evt);
    }

    protected emitInserted(key: string, newValue: string) {
        const evt = new StorageEvent('storage', {'key': key, 'newValue': newValue, 'oldValue': null});
        this.dispatchEvent(evt);
    }

    protected emitUpdated(key: string, newValue: string, oldValue: string) {
        const evt = new StorageEvent('storage', {'key': key, 'newValue': newValue, 'oldValue': oldValue});
        this.dispatchEvent(evt);
    }

    clear(): void {
        this.kv.forEach( (value, key) => {
            this.emitDeleted(key, value);
        });
        this.kv.clear();
        this.length = this.kv.size;
    }

    getItem(key: string): string | null {
        return this.kv.get(key) || null;
    }

    key(index: number): string | null {
        const keys = Array.from(this.kv.keys());
        return keys[index] || null;
    }

    removeItem(key: string): void {
        const oldValue = this.kv.get(key);
        this.kv.delete(key);
        this.length = this.kv.size;
        if (oldValue) {
            this.emitDeleted(key, oldValue);
        } else {
            // Do nothing. Per spec: "If there is no item associated with the given key, this method will do nothing."
        }
    }

    setItem(key: string, newValue: string): void {
        const oldValue = this.kv.get(key)

        if(oldValue) {
            this.kv.set(key, newValue);
            this.emitUpdated(key, newValue, oldValue);
        } else {
            this.kv.set(key, newValue);
            this.length = this.kv.size;
            this.emitInserted(key, newValue);
        }
    }

}