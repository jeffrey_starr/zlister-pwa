/* Interface definitions for APIs that do not exist in node and jsdom */

export class StorageManager {
    estimate(): Promise<{}> { return Promise.reject('not implemented'); }

    getDirectory(): Promise<FileSystemDirectoryHandle> { return Promise.reject('not implemented'); }

    persist(): Promise<boolean> { return Promise.reject('not implemented'); }

    persisted(): Promise<boolean> { return Promise.reject('not implemented'); }
}

export class CacheStorage {
    match(request, options): Promise<any> { return Promise.reject('not implemented'); }

    has(cacheName): Promise<boolean> { return Promise.reject('not implemented'); }

    open(cacheName): Promise<Cache> { return Promise.reject('not implemented'); }

    delete(cacheName): Promise<boolean> { return Promise.reject('not implemented'); }

    keys(): Promise<string[]> { return Promise.reject('not implemented'); }
}

export class Cache {
    match(request, options): Promise<any> { return Promise.reject('not implemented'); }

    matchAll(request?, options?): Promise<any> { return Promise.reject('not implemented'); }

    add(request): Promise<void> { return Promise.reject('not implemented'); }

    addAll(requests): Promise<void> { return Promise.reject('not implemented'); }

    put(request, response): Promise<void> { return Promise.reject('not implemented'); }

    delete(request, options?): Promise<boolean> { return Promise.reject('not implemented'); }

    keys(request?, options?): Promise<any> { return Promise.reject('not implemented'); }
}

export class Response {
    constructor(body?, options?) {
        this.body = body;
        this.status = options.status || 599;
        this.ok = this.status >= 200 && this.status < 300;

        this.headers = new Headers(options.headers || []);
    }

    readonly body;

    readonly bodyUsed: boolean;

    readonly headers: Headers;

    readonly ok: boolean;

    readonly redirected: boolean;

    readonly status;

    readonly statusText;

    readonly type;

    readonly url: string;
}

export class Headers {

    internal: Map<string, string>

    constructor(init?) {
        this.internal = new Map();
        for(const pair of init) {
            this.internal.set(pair[0], pair[1]);
        }
    }

    get(name): string {
        return this.internal.get(name);
    }
}