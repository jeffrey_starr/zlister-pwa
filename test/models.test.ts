import {ZList, ZListEntry} from "../src/models";
import {validate as uuidvalidate, v4 as uuidv4} from 'uuid';

describe('ZListEntry', () => {
    describe('constructor', () => {
        test('fully populated, all valid props', () => {
            let exampleUUID = '51a5bb22-fad2-4dd7-92d8-7beb52cd5b76'
            let props = {'id': exampleUUID, 'content': 'content', 'isChecked': true, 'isDeleted': true, 'lastUpdated': Date.parse("2020-12-31")};
            let entry = new ZListEntry(props);

            expect(entry.id).toBe(props['id']);
            expect(entry.content).toBe(props['content']);
            expect(entry.isChecked).toBe(props['isChecked']);
            expect(entry.isDeleted).toBe(props['isDeleted']);
            expect(entry.lastUpdated).toBe(props['lastUpdated']);
        });

        test('minimally populated, all valid data', () => {
            let props = {'content': 'content'};
            let now = Date.now();
            let entry = new ZListEntry(props);

            expect(entry.content).toBe(props['content']);
            expect(uuidvalidate(entry.id)).toBe(true);
            expect(entry.isChecked).toBe(false);
            expect(entry.isDeleted).toBe(false);
            expect(entry.lastUpdated.valueOf()).toBeGreaterThanOrEqual(now.valueOf());
        });

        test('content invalid, error thrown', () => {
            let propsNotAString = {'content': 5};
            let propsEmptyString = {'content': ''};
            let propsNoValue = {'id': 7};

            expect(() => new ZListEntry(propsNotAString)).toThrow();
            expect(() => new ZListEntry(propsEmptyString)).toThrow();
            expect(() => new ZListEntry(propsNoValue)).toThrow();
        })
    });

    describe('serialize', () => {
        test('round trips with constructor', () => {
            let exampleUUID = '51a5bb22-fad2-4dd7-92d8-7beb52cd5b76'
            let props = {'id': exampleUUID, 'content': 'content', 'isChecked': true, 'isDeleted': true, 'lastUpdated': Date.parse("2020-12-31")};
            let entry = new ZListEntry(props);

            let parsedProps = JSON.parse(entry.serialize());
            expect(parsedProps).toEqual(props);
            let parsedEntry = new ZListEntry(parsedProps);
            expect(parsedEntry).toStrictEqual(entry);
        })
    });
});

describe('ZList', () => {
    describe('constructor', () => {
        test('fully populated, no children, all valid props', () => {
            const exampleUUID = 'd4ab825a-fb69-4aca-90c0-02b1b71010fb';
            const props = {'id': exampleUUID, 'title': 'Title', 'isDeleted': true, 'children': []};

            const zlist = new ZList(props);
            expect(zlist.id).toBe(exampleUUID);
            expect(zlist.title).toBe('Title');
            expect(zlist.isDeleted).toBe(true);
            expect(zlist.children).toHaveLength(0);
        });

        test('minimally populated, no children, all valid props', () => {
            const props = {'title': 'Title'};

            const zlist = new ZList(props);
            expect(uuidvalidate(zlist.id)).toBe(true);
            expect(zlist.title).toBe('Title');
            expect(zlist.isDeleted).toBe(false);
            expect(zlist.children).toHaveLength(0);
        });
    });

    describe('entry access methods', () => {
        test('entries are filtered by condition', () => {
            const zlist = new ZList({'title': 'Title'});
            zlist.children.push(new ZListEntry({'content': 'Completed', 'isChecked': true}));
            zlist.children.push(new ZListEntry({'content': 'To Do'}));
            zlist.children.push(new ZListEntry({'content': 'Deleted', 'isDeleted': true}));

            expect(zlist.completed()).toHaveLength(1);
            expect(zlist.deleted()).toHaveLength(1);
            expect(zlist.toDo()).toHaveLength(1);

            expect(zlist.completed()[0].content).toBe('Completed');
            expect(zlist.deleted()[0].content).toBe('Deleted');
            expect(zlist.toDo()[0].content).toBe('To Do');
        });

        test('entry - lookup by id', () => {
            const zlist = new ZList({'title': 'Title'});
            const a = new ZListEntry({'content': 'A Entry'});
            const b = new ZListEntry({'content': 'B Entry'});
            zlist.children.push(a);
            zlist.children.push(b);

            expect(zlist.entry(a.id)).toBe(a);
            expect(zlist.entry(b.id)).toBe(b);
            expect(zlist.entry(uuidv4())).toBeNull();
        });
    });

    describe('serialize', () => {
        test('round trips with constructor', () => {
            const zlist = new ZList({'title': 'Title'});
            zlist.children.push(new ZListEntry({'content': 'First Child'}));
            zlist.children.push(new ZListEntry({'content': 'Second Child'}));
            const serialized = zlist.serialize();

            const actual = new ZList(JSON.parse(serialized));
            expect(actual).toStrictEqual(zlist);
        });
    });

    describe('upsert', () => {
        test('replace if entry.id already exists', () => {
            const zlist = new ZList({title: 'Title'});
            const child = new ZListEntry({content: 'Original'});
            zlist.upsert(child);

            const updated = new ZListEntry({id: child.id, content: 'Updated'});
            zlist.upsert(updated);

            expect(zlist.children).toHaveLength(1);
            expect(zlist.children[0].content).toBe('Updated');
        });

        test('append if entry.id does not exist', () => {
            const zlist = new ZList({title: 'Title'});
            const child = new ZListEntry({content: 'Original'});
            zlist.upsert(child);

            const neverSeen = new ZListEntry({content: 'Updated'});
            zlist.upsert(neverSeen);

            expect(zlist.children).toHaveLength(2);
        });
    });
});
