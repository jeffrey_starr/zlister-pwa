import {instance, mock, reset, verify, when} from "ts-mockito";
import {StorageManager as DefStorageManager, CacheStorage as DefCacheStorage, Cache as DefCache, Response as DefResponse} from "./defs";
import {CacheStorageSvc, DefaultZListMgmt, ImportResult, PersistencePolicy, StorageZListDAO, ZListDAO, ZListStorageEvent} from "../src/services";
import {ShimStorage} from "./ShimStorage";
import {ZList, ZListEntry} from "../src/models";

describe('PersistencePolicy', () => {
    const managerMock = mock(DefStorageManager);

    describe('browserWillAvoidDeletion', () => {
        test('passes along check', async () => {
            const policy = new PersistencePolicy(instance(managerMock));

            when(managerMock.persisted()).thenResolve(true);
            const actual1 = await policy.browserWillAvoidDeletion();
            expect(actual1).toBe(true);

            when(managerMock.persisted()).thenResolve(false);
            const actual2 = await policy.browserWillAvoidDeletion();
            expect(actual2).toBe(false);
        });
    });

    describe('promptToAvoidDeletion', () => {
        test('passes along request result', async () => {
            const policy = new PersistencePolicy(instance(managerMock));

            when(managerMock.persist()).thenResolve(true);
            const actual1 = await policy.promptToAvoidDeletion();
            expect(actual1).toBe(true);

            when(managerMock.persist()).thenResolve(false);
            const actual2 = await policy.promptToAvoidDeletion();
            expect(actual2).toBe(false);
        });
    });
});

describe('StorageZListDAO', () => {

    describe('creation, read, updates', () => {
        test('add and update one zlist', () => {
            const storage = new ShimStorage();
            const dao = new StorageZListDAO({'storage': storage, 'target': storage});
            const events: Array<ZListStorageEvent> = [];

            dao.addEventListener('zliststorageevent', (evt) => { events.push(evt); } )

            expect(dao.lists()).toHaveLength(0);

            const zlist = new ZList({'title': 'Title'});

            dao.upsertList(zlist)

            expect(dao.lists()).toHaveLength(1);
            expect(dao.lists()[0]).toEqual(zlist);
            expect(events[0]).toEqual(new ZListStorageEvent(zlist.id, zlist, null));
            expect(dao.getList(zlist.id)).toEqual(zlist);

            const entry = new ZListEntry({'content': 'Task'});
            zlist.children.push(entry);

            // pre-database update, so they should be different
            const oldZlist = dao.getList(zlist.id);
            expect(zlist !== oldZlist).toBe(true);

            dao.upsertList(zlist);

            // post-database update, so they should be the same
            expect(dao.getList(zlist.id)).toEqual(zlist);
            expect(events[1]).toEqual(new ZListStorageEvent(zlist.id, zlist, oldZlist));
        });

        test('getList is null if key does not exist', () => {
            const storage = new ShimStorage();
            const dao = new StorageZListDAO({'storage': storage, 'target': storage});

            expect(dao.getList('does-not-exist')).toBeNull();
        });
    });

    describe('creation, read, deletes', () => {
        test('add and delete one zlist', () => {
            const storage = new ShimStorage();
            const dao = new StorageZListDAO({'storage': storage, 'target': storage});
            const events: Array<ZListStorageEvent> = [];

            dao.addEventListener('zliststorageevent', (evt) => { events.push(evt); } )

            expect(dao.lists()).toHaveLength(0);

            const zlist = new ZList({'title': 'Title'});

            dao.deleteList(zlist.id);

            expect(events).toHaveLength(0); // since zlist was never added, the delete should be silently ignored

            dao.upsertList(zlist);
            expect(dao.lists()).toHaveLength(1);

            dao.deleteList(zlist.id);

            expect(dao.lists()).toHaveLength(0);
            expect(events[1]).toEqual(new ZListStorageEvent(null, null, zlist));
        });
    });

});

describe('DefaultZListMgmt', () => {
    const mockDao = mock<ZListDAO>();
    const testZList = new ZList({'title': 'Test ZList'});

    describe('export', () => {
        afterEach(() => {
            reset(mockDao);
        });

        test('minimal blob if there are no entries',  () => {
            const mgmt = new DefaultZListMgmt(instance(mockDao));

            when(mockDao.lists()).thenReturn([]);

            const content = mgmt.export();

            expect(JSON.parse(content)).toStrictEqual({zLists: []});
        });

        test('serializes zlists', () => {
            const mgmt = new DefaultZListMgmt(instance(mockDao));
            const a = new ZList({title: 'A'});
            const b = new ZList({title: 'B'});

            when(mockDao.lists()).thenReturn([a, b]);

            const content = mgmt.export();

            expect(JSON.parse(content)).toStrictEqual({zLists: [
                JSON.parse(a.serialize()), JSON.parse(b.serialize())
                ]});
        });
    });

    describe('import', () => {
        // test cases:
        // - non-json content
        // - json, but lacks zList field
        // - partial import; one or more array elements invalid
        // - full import / happy path
        // since we have import logic within the models, we do not need to replicate it here

        test('non-json content', () => {
            const mgmt = new DefaultZListMgmt(instance(mockDao));

            expect(mgmt.import('foo')).toStrictEqual(new ImportResult(0, [new SyntaxError('Unexpected token \'o\', "foo" is not valid JSON')]))
        });

        test('lacks zList field', () => {
            const mgmt = new DefaultZListMgmt(instance(mockDao));

            const emptyObject = "{}";
            const emptyArray = "[]";
            const otherobject = "{\"foo\": 1}";
            const wrongType = "{\"zLists\": {}}";

            for(let test of [emptyObject, emptyArray, otherobject, wrongType]) {
                expect(mgmt.import(test)).toStrictEqual(new ImportResult(0, [Error('zLists property does not exist or is not an array')]));
            }
        });

        test('imports partial', () => {
            const mgmt = new DefaultZListMgmt(instance(mockDao));

            const goodInputs = [new ZList({title: 'Foo'}), new ZList({title: 'Bar'})];
            const withBad = `{"zLists": [${goodInputs[0].serialize()}, "baz", ${goodInputs[1].serialize()}]}`;

            when(mockDao.upsertList(goodInputs[0])).thenReturn();
            when(mockDao.upsertList(goodInputs[1])).thenReturn()

            expect(mgmt.import(withBad)).toStrictEqual(new ImportResult(2, [TypeError("Cannot use 'in' operator to search for 'title' in baz")]));
        });

        test('full import', () => {
            const mgmt = new DefaultZListMgmt(instance(mockDao));

            const goodInputs = [new ZList({title: 'Foo'}), new ZList({title: 'Bar'})];
            const onlyGood = `{"zLists": [${goodInputs[0].serialize()}, ${goodInputs[1].serialize()}]}`;

            when(mockDao.upsertList(goodInputs[0])).thenReturn();
            when(mockDao.upsertList(goodInputs[1])).thenReturn()

            expect(mgmt.import(onlyGood)).toStrictEqual(new ImportResult(2, []));
        });
    });

    describe('list', () => {
        afterEach(() => {
            reset(mockDao);
        });

        test('get list by id, exists', () => {
            const mgmt = new DefaultZListMgmt(instance(mockDao));

            when(mockDao.getList(testZList.id)).thenReturn(testZList);

            expect(mgmt.list(testZList.id)).toBe(testZList);
        });

        test('get list by id returns null if id does not exist', () => {
            const mgmt = new DefaultZListMgmt(instance(mockDao));

            when(mockDao.getList(testZList.id)).thenReturn(null);

            expect(mgmt.list(testZList.id)).toBeNull();
        });
    });

    describe('lists', () => {
        afterEach(() => {
            reset(mockDao);
        });

        test('empty list if there are no entries', () => {
            const mgmt = new DefaultZListMgmt(instance(mockDao));

            when(mockDao.lists()).thenReturn([]);

            expect(mgmt.lists()).toStrictEqual([]);
        });

        test('empty list if all entries are deleted', () => {
            const mgmt = new DefaultZListMgmt(instance(mockDao));

            when(mockDao.lists()).thenReturn([new ZList({title: 'T', isDeleted: true})]);

            expect(mgmt.lists()).toStrictEqual([]);
        });

        test('list is alphabetically sorted for all non-deleted entries', () => {
            const mgmt = new DefaultZListMgmt(instance(mockDao));

            const a = new ZList({title: 'A'});
            const b = new ZList({title: 'B'});

            when(mockDao.lists()).thenReturn([b, a]);

            expect(mgmt.lists()).toStrictEqual([a, b]);
        });
    });

    describe('makeNewList', () => {
        test('return Error if title is empty', () => {
            const mgmt = new DefaultZListMgmt(instance(mockDao));

            expect(mgmt.makeNewList('')).toBeInstanceOf(Error);
        });

        test('upsert valid ZList and return', () => {
            const mgmt = new DefaultZListMgmt(instance(mockDao));

            const zlist = mgmt.makeNewList('Title')
            if (zlist instanceof ZList) {
                verify(mockDao.upsertList(zlist)).called();
                expect(zlist.title).toBe('Title');
            } else {
                fail('zlist should not be an Error for this test');
            }
        });
    });

    describe('makeNewEntry', () => {
        test('if zlistid does not exist, return Error', () => {
            const mgmt = new DefaultZListMgmt(instance(mockDao));

            when(mockDao.getList('does-not-exist')).thenReturn(null);

            const entry = mgmt.makeNewEntry('does-not-exist', 'content')

            expect(entry).toBeInstanceOf(Error);
        });

        test('zlist exists, add to zlist and return new entry', () => {
            const mgmt = new DefaultZListMgmt(instance(mockDao));
            const zlist = new ZList({title: 'Exists'});

            when(mockDao.getList('exists')).thenReturn(zlist);

            const entry = mgmt.makeNewEntry('exists', 'content')
            verify(mockDao.upsertList(zlist)).called();

            expect(entry).toBeInstanceOf(ZListEntry)
            if(entry instanceof ZListEntry) {
                expect(entry.content).toBe('content');
            }
        });
    });

    describe('sync', () => {
        test('if zlistid does not exist, return Error', () => {
            const mgmt = new DefaultZListMgmt(instance(mockDao));

            when(mockDao.getList('does-not-exist')).thenReturn(null);

            const entry = mgmt.sync('does-not-exist', new ZListEntry({content: 'DK'}));

            expect(entry).toBeInstanceOf(Error);
        });

        test('zlist exists, add entry to zlist and commit zlist to database', () => {
            const mgmt = new DefaultZListMgmt(instance(mockDao));
            const zlist = new ZList({title: 'T'});
            const entry = new ZListEntry({content: 'C'});

            expect(zlist.children.length).toBe(0);
            when(mockDao.getList('exists')).thenReturn(zlist);

            const synced = mgmt.sync('exists', entry);
            expect(synced).toBe(true);

            verify(mockDao.upsertList(zlist)).called();
            expect(zlist.children).toContain(entry);
        });
    });
});

describe('CacheStorageSvc',  () => {
    const storage = mock(DefCacheStorage);
    const cache = mock(DefCache);

    describe('clear', () => {
        test('calls clear on cache storage', async () => {
            when(storage.open('zlister')).thenResolve(instance(cache));
            const cacheSvc = new CacheStorageSvc(instance(storage));

            when(storage.delete('zlister')).thenResolve(true);
            const found = await cacheSvc.clear();
            expect(found).toBe(true);

            when(storage.delete('zlister')).thenResolve(false);
            const found2 = await cacheSvc.clear();
            expect(found2).toBe(false);
        });
    })

    describe('size',  () => {
        test('if cache is empty, return zero', async () => {
            when(storage.open('zlister')).thenResolve(instance(cache));
            const cacheSvc = new CacheStorageSvc(instance(storage));

            when(cache.keys()).thenResolve([]);
            const size = await cacheSvc.size();
            expect(size).toBe(0);
        });

        test('if cache is non-empty, return size', async () => {
            when(storage.open('zlister')).thenResolve(instance(cache));
            const cacheSvc = new CacheStorageSvc(instance(storage));

            when(cache.keys()).thenResolve(['foo', 'bar']);
            const size = await cacheSvc.size();
            expect(size).toBe(2);
        });
    });

    describe('failedResponses', () => {
        test('return only responses not ok', async () => {
            when(storage.open('zlister')).thenResolve(instance(cache));
            const cacheSvc = new CacheStorageSvc(instance(storage));

            const allResponses = [
                new DefResponse(null, {status: 200}),
                new DefResponse(null, {status: 400}),
                new DefResponse(null, {status: 500})];

            when(cache.matchAll()).thenResolve(allResponses);
            const failed = await cacheSvc.failedResponses();
            expect(failed.length).toBe(2);
            expect(failed[0]).toStrictEqual(allResponses[1]);
            expect(failed[1]).toStrictEqual(allResponses[2]);
        });
    });

    describe('youngest', () => {
        test('fail the promise if there are no cache entries', async () => {
            when(storage.open('zlister')).thenResolve(instance(cache));
            const cacheSvc = new CacheStorageSvc(instance(storage));

            when(cache.matchAll()).thenResolve([]);
            await expect(cacheSvc.youngest()).rejects.toBeInstanceOf(Error);
        });

        test('return the youngest / latest response by Date field', async () => {
            when(storage.open('zlister')).thenResolve(instance(cache));
            const cacheSvc = new CacheStorageSvc(instance(storage));

            const allResponses = [
                new DefResponse(null, {status: 200, headers: [['Date', 'Wed, 21 Oct 2015 15:28:00 GMT']]}),
                new DefResponse(null, {status: 200, headers: [['Date', 'Wed, 21 Oct 2015 05:28:00 GMT']]}),
                new DefResponse(null, {status: 200, headers: [['Date', 'Wed, 21 Oct 2015 10:28:00 GMT']]})
            ];

            when(cache.matchAll()).thenResolve(allResponses);
            await expect(cacheSvc.youngest()).resolves.toStrictEqual(new Date(2015, 9, 21, 15, 28, 0));
        });

        test('return the youngest response, ignoring missing headers and improperly formatted dates', async () => {
            when(storage.open('zlister')).thenResolve(instance(cache));
            const cacheSvc = new CacheStorageSvc(instance(storage));

            const allResponses = [
                new DefResponse(null, {status: 200, headers: [['Date', 'Wed, 21 Oct 2015 15:28:00 UTC']]}),  // invalid
                new DefResponse(null, {status: 200, headers: [['Date', 'Wed, 21 Oct 2015 05:28:00 GMT']]}),
                new DefResponse(null, {status: 200, headers: [['Date', 'Wed, 21 Oct 2015 10:28:00 GMT']]}),
                new DefResponse(null, {status: 200}) // missing
            ];

            when(cache.matchAll()).thenResolve(allResponses);
            await expect(cacheSvc.youngest()).resolves.toStrictEqual(new Date(2015, 9, 21, 10, 28, 0));
        });
    });

    describe('httpDateToDate', () => {
        test('parse an invalid date, returning error', () => {
            expect(CacheStorageSvc.httpDateToDate('Tue Jul 16 10:42:36 AM MDT 2024')).toBeInstanceOf(Error);
        });

        test('parse valid date', () => {
            const actual = 'Wed, 21 Oct 2015 07:28:00 GMT';
            const expected = new Date(2015, 9, 21, 7, 28, 0);

            expect(CacheStorageSvc.httpDateToDate(actual)).toStrictEqual(expected);
        });
    })
});
